package com.example.chasebanknycschoolschallenge;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.os.Bundle;
import android.util.Log;
/*
NYC Schools Challenge Example for Chase Bank
Description: Create an iOS app that provides information on NYC High schools. Display a list of NYC High
Schools.
School List Source API:
!! Get your data here - https://data.cityofnewyork.us/Education/DOE-High-School-Directory-2017/s3k6-
pzi2
SAT Source API:
!! SAT data here - https://data.cityofnewyork.us/Education/SAT-Results/f9bf-2cp4
UI Note: Selecting a school should show additional information about the school. Display all the SAT
scores - include Math, Reading and Writing.
What are we looking for from this exercise?
a) A fully working prototype.
b) Don’t worry about making the UI pretty.
c) Tech approach you took and why.
d) What was important to you – look and feel or the functionality?
e) Unit test
 */
public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private static MainActivity instance;
    RestAPIClient schoolDataClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instance = this;

        //Preliminary Unit Tests
        RestAPIClient.UnitTestRestAPIClient();
        SchoolDataViewActivity.unitTestStringFilter();

        //Instantiations
        schoolDataClient = new RestAPIClient(this);
    }

    protected void initRecyclerView(){
        //A recycler view allows us to view a large dataset efficiently.

        Log.d(TAG, "initRecyclerView()");
        RecyclerView recyclerView = findViewById(R.id.recyclerAllSchools);
        RecyclerViewAdapterAllSchools adapter = new RecyclerViewAdapterAllSchools(this, schoolDataClient);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    //This method makes it easy to run MainActivity methods from subclasses.
    public static MainActivity getInstance(){
        return instance;
    }
}
