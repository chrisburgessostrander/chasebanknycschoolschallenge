package com.example.chasebanknycschoolschallenge;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

public class SchoolDataViewActivity extends AppCompatActivity {
    private static final String TAG = "SchoolDataViewActivity";
    RestAPIClient apiClient;

    TextView txtSchoolName;
    TextView txtFldSATNum;
    TextView txtFldSATMath;
    TextView txtFldSATRead;
    TextView txtFldSATWrite;
    TextView txtSATNum;
    TextView txtSATMath;
    TextView txtSATRead;
    TextView txtSATWrite;
    TextView txtSATUnavailable;
    TextView txtOverView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_data_view);
        this.apiClient = RestAPIClient.getInstance();
        JSONObject satData = apiClient.getCursorSATData();
        JSONObject school = apiClient.getCursorSchool();
        String overview;
        try {
            overview = filterOverViewString(school.getString("overview_paragraph"));
        } catch (JSONException e) {
            e.printStackTrace();
            overview = "";
        }
        txtSchoolName = findViewById(R.id.textSchoolDataViewTitle);
        txtFldSATMath = findViewById(R.id.textFieldSATMath);
        txtFldSATRead = findViewById(R.id.textFieldSATReading);
        txtFldSATWrite = findViewById(R.id.textFieldSATWriting);
        txtFldSATNum = findViewById(R.id.textFieldSATNum);

        txtSATMath = findViewById(R.id.textSATScoreMath);
        txtSATRead = findViewById(R.id.textSATScoreReading);
        txtSATWrite = findViewById(R.id.textSATScoreWriting);
        txtSATNum = findViewById(R.id.textSATNum);

        txtSATUnavailable = findViewById(R.id.textSatUnavailable);

        txtOverView = findViewById(R.id.textSchoolOverview);
        try {
            if(satData != null){

                txtSchoolName.setText(satData.getString("school_name"));
                txtFldSATNum.setText(satData.getString("num_of_sat_test_takers") + "    ");
                txtFldSATMath.setText(satData.getString("sat_math_avg_score") + "    ");
                txtFldSATRead.setText(satData.getString("sat_critical_reading_avg_score") + "    ");
                txtFldSATWrite.setText(satData.getString("sat_writing_avg_score") + "    ");
                txtOverView.setText(overview);
                txtOverView.setVisibility(View.VISIBLE);
                txtFldSATNum.setVisibility(View.VISIBLE);
                txtFldSATMath.setVisibility(View.VISIBLE);
                txtFldSATRead.setVisibility(View.VISIBLE);
                txtFldSATWrite.setVisibility(View.VISIBLE);
                txtSATNum.setVisibility(View.VISIBLE);
                txtSATMath.setVisibility(View.VISIBLE);
                txtSATRead.setVisibility(View.VISIBLE);
                txtSATWrite.setVisibility(View.VISIBLE);

                txtSATUnavailable.setVisibility(View.INVISIBLE);
            }else{
                txtSchoolName.setText(school.getString("school_name"));
                txtOverView.setVisibility(View.INVISIBLE);
                txtFldSATNum.setVisibility(View.INVISIBLE);
                txtFldSATMath.setVisibility(View.INVISIBLE);
                txtFldSATRead.setVisibility(View.INVISIBLE);
                txtFldSATWrite.setVisibility(View.INVISIBLE);
                txtSATNum.setVisibility(View.INVISIBLE);
                txtSATMath.setVisibility(View.INVISIBLE);
                txtSATRead.setVisibility(View.INVISIBLE);
                txtSATWrite.setVisibility(View.INVISIBLE);

                txtSATUnavailable.setVisibility(View.VISIBLE);
                txtSATUnavailable.setText("SAT Data Unavailable\n\n" + overview);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }    }

        private String filterOverViewString(String overView){
            String filteredString = overView.replace("Â","");
            return filteredString;
        }

        public static void unitTestStringFilter(){
        String garbledString = "HeÂlloÂ WorÂld";
        String filteredString = garbledString.replace("Â","");
        Log.d(TAG,"Filtered String: " + filteredString);
        }
}
