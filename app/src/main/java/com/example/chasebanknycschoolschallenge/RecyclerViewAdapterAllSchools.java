package com.example.chasebanknycschoolschallenge;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONException;
import org.json.JSONObject;

public class RecyclerViewAdapterAllSchools extends  RecyclerView.Adapter<RecyclerViewAdapterAllSchools.ViewHolder> {
    private static final String TAG = "RecyclerViewAdapterAllSchools";
    private RestAPIClient restAPIClient;
    public RecyclerViewAdapterAllSchools(Context context, RestAPIClient apiClient){
        this.restAPIClient = apiClient;
    }

    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_listitem, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Log.d(TAG,"onBindViewHolder: called.");
        //Bind next JSONArray element as JSONObject to the new ViewHolder.
        try {
            JSONObject school = restAPIClient.schoolList.getJSONObject(position);
            String schoolName = school.getString("school_name");
            holder.schoolName.setText(schoolName);
            Log.d(TAG,"Loading " + school.getString("school_name"));

            //On Click, query URL for SAT Data about the selected school.
            holder.parentLayout.setOnClickListener(new SchoolDataOnClickListener(school){
                @Override
                public void onClick(View view) {
                    try {
                        restAPIClient.setCursorSchool(school);
                        restAPIClient.getSATData(school);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return restAPIClient.schoolList.length();
    }


    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView schoolName;
        ConstraintLayout parentLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            schoolName = itemView.findViewById(R.id.school_name);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }
}
