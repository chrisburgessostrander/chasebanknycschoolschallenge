package com.example.chasebanknycschoolschallenge;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class RestAPIClient {
    private static final String TAG = "RestAPIClient";
    protected JSONArray schoolList;
    private JSONObject cursorSATData;
    private JSONObject cursorSchool;
    private static RestAPIClient instance;
    RequestQueue queue;
    Cache cache;
    Network network = new BasicNetwork(new HurlStack());

    public RestAPIClient(Context context) {
        instance = this;
        cache = new DiskBasedCache(context.getCacheDir(), 1024 * 1024); //1MB cap
        getSchoolData();
        if(queue == null){
            queue = new RequestQueue(cache, network);
            queue.start();
        }
    }

    public static RestAPIClient getInstance(){
        return instance;
    }


    protected void setCursorSATData(JSONObject satData){
        this.cursorSATData = satData;
    } protected JSONObject getCursorSATData(){ return this.cursorSATData;}

    protected void setCursorSchool(JSONObject school){
        this.cursorSchool = school;
    } protected JSONObject getCursorSchool(){ return this.cursorSchool;}

    //Get a list of all schools.
    private void getSchoolData() {
        if(queue == null){
            queue = new RequestQueue(cache, network);
            queue.start();
        }
        Log.d(TAG,"Getting School Data");
        //Select the dbn, school_name, overview_paragraph and phone_number fields from the API.
        String url = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json?$select=" +
                "dbn," +
                "school_name," +
                "overview_paragraph," +
                "phone_number"; //phone_number is unused. I chose not to remove it as I would like to add contact list fields.

        JsonArrayRequest schoolDataRequest = new JsonArrayRequest(
                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                //Save the list of schools in memory.
                schoolList = response;
                //getInstance() to get access to MainActivity's instance methods.
                MainActivity.getInstance().initRecyclerView();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                System.out.println("Error " + error.toString());
            }
        });
        queue.add(schoolDataRequest);
        Log.d(TAG,"schoolDataRequest added to queue");
    }

    //Request data about the SAT scores at a given school.
    protected void getSATData(final JSONObject school) throws JSONException {
        if(queue == null){
            queue = new RequestQueue(cache, network); //Intent issues??
            queue.start();
        }

        //Get only the SAT data associated with a given school, queried by database number.
        String dbn = school.getString("dbn");
        String schoolName = school.getString("school_name");
        String url = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json?dbn=" + dbn;
        JsonArrayRequest satDataRequest = new JsonArrayRequest(

                Request.Method.GET,
                url,
                null,
                new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    //Not all "dbn"s will return a response! No record exists for these schools.
                    if(response.length()>0){
                        JSONObject satData = response.getJSONObject(0);
                        setCursorSATData(satData);
                    }else{
                        //If setCursorSATData is not set to null, the previously accessed object will be used instead.
                        setCursorSATData(null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                //Launch new Activity.
                Intent schoolDataView = new Intent(MainActivity.getInstance(),SchoolDataViewActivity.class);
                MainActivity.getInstance().startActivity(schoolDataView);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(satDataRequest);
        Log.d(TAG,"SATDataRequest added to queue for: " + schoolName + " at: " + url);
    }

    protected static void UnitTestRestAPIClient(){
        Cache testCache = new DiskBasedCache(MainActivity.getInstance().getCacheDir(), 1024 * 1024); //1MB cap
        Network testNetwork = new BasicNetwork(new HurlStack());
        RequestQueue testQueue = new RequestQueue(testCache, testNetwork);
        testQueue.start();

        String testURL1 = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json";
        String testURL2 = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

        JsonArrayRequest testRequest1 = new JsonArrayRequest(
                Request.Method.GET,
                testURL1,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        //For some reason, (int i = 0; i < response.length(); i++) did not work here.
                        int i = 0;
                        while(i < response.length()){
                            try {
                                Log.d(TAG, "Downloading school data for " + response.getJSONObject(i).getString("school_name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            i++;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"Error " + error.toString());
            }
        });

        JsonArrayRequest testRequest2 = new JsonArrayRequest(
                Request.Method.GET,
                testURL2,
                null,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        int i = 0;
                        while(i < response.length()){
                            try {
                                Log.d(TAG, "Downloading SAT data for " + response.getJSONObject(i).getString("school_name"));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            i++;
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG,"Error " + error.toString());
            }
        });
        testQueue.add(testRequest1);
        testQueue.add(testRequest2);
    Log.d(TAG,"testRequest1 && testRequest2 added to testQueue");
    }
}

